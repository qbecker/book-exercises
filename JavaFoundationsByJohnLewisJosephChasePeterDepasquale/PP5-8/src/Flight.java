
public class Flight {
	//instnace varables
	private String name, destination, origin;
	private int flightNumber;
	
	
	//main constructor
	public Flight(String name, String destination, String origin, int flightNumber){
		this.name = name;
		this.destination = destination;
		this.origin = origin;
		this.flightNumber = flightNumber;
	}
	
	//getters
	public String getName(){
		return name;
	}
	
	public String getDestination(){
		return destination;
	}
	
	public String getOrigin(){
		return origin;
	}
	
	public int getFlightNumber(){
		return flightNumber;
	}
	
	
	//setters
	public void setName(String name){
		this.name = name;
	}
	
	public void setDestination(String destination){
		this.destination = destination;
	}
	
	public void setOrigin(String origin){
		this.origin = origin;
	}
	
	public void setFlightNumber(int flightNumber){
		this.flightNumber = flightNumber;
	}
	
	//tostring
	public String toString(){
		String result = "";
		result += "Flight name: "+name+"\n";
		result += "Flight orgin: "+origin+"\n";
		result += "Flight destination: "+destination+"\n";
		result += "Flight number: "+flightNumber+"\n";
		return result;
	}
}
