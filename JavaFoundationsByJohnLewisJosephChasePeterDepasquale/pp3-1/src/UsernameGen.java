import java.util.Random;
import java.util.Scanner;

public class UsernameGen {
	
	public static void main(String args[]){
		//decalare varables
		String userName, firstName, lastName, firstLetterLastName, randomAsString, firstLetterAsString;
		char firstLetter;
		int random;
		
		//open scanner
		Scanner scan = new Scanner(System.in);
		
		//open random number gen and get a number from it
		Random generator = new Random();
		random = generator.nextInt(10)+1;
		// take input
		System.out.println("please enter your first name: ");
		firstName = scan.next();
		System.out.println("please enter your last name: ");
		lastName = scan.next();
		firstLetter = firstName.charAt(0);
		System.out.println(firstLetter);
		
		firstLetterAsString = Character.toString(firstLetter);
		randomAsString = String.valueOf(random);
		firstLetterLastName = firstLetterAsString.concat(lastName);
		userName = firstLetterLastName.concat(randomAsString);
		
		System.out.println("Your new Username is: "+userName);
		
		
		
		
		
		scan.close();
	}

}
