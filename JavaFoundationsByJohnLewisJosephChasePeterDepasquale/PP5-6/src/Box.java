
public class Box {
	
	//instance varables
	private double height, width, depth;
	boolean full;
	
	public Box(double height, double width, double depth){
		this.full = false;
		this.height = height;
		this.width = width;
		this.depth = depth;
	}
	
	public double getHeight(){
		return height;
	}
	
	public double getWidth(){
		return width;
	}
	
	public double getDepth(){
		return depth;
	}
	public boolean getStatus(){
		return full;
	}
	public void setHeight(){
		this.height = height;
	}
	
	public void setWidth(){
		this.width = width;
	}
	
	public void setDepth(){
		this.depth = depth;
	}
	
	public void setFull(boolean full){
		this.full = full;
	}
	
	public String toString(){
		return "depth: "+getDepth()+" height: "+getHeight()+" width: "+getWidth()+" is the box full?: "+getStatus();
	}
	
}
