import java.util.*;

public class coinJar {
	public static void main (String args[]){
		
		int pennie, nickle, dime, quarter;//ints of all available types of coins for input
		double outPut;
		
		
		Scanner input = new Scanner(System.in);  //open input scanner
		
		System.out.println("|===============================|");
		System.out.println("|        Coin jar               |");
		System.out.println("|===============================|");
		
		System.out.println("Enter number of quarters in the jar: ");
		
		quarter = input.nextInt();
		
		System.out.println("Enter number of nickles in the jar: ");
		
		nickle = input.nextInt();
		
		System.out.println("Enter number of dimes in the jar: ");
		
		dime = input.nextInt();
		
		System.out.println("Enter number of pennies in the jar: ");
		
		pennie = input.nextInt();
		
		outPut = ((double) (quarter*.25))+((double) (pennie*.01))+((double) (nickle*.05))+((double) (dime*.10));
		
		System.out.println("Total dollar ammount in jar: $"+outPut);
		
		input.close();
	}

}
