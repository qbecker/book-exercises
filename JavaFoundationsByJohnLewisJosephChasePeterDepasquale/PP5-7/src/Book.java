
public class Book {
	
	//instance varables
	private String title, author, publisher;
	private int copyWriteDate;
	
	
	//main constructor
	public Book(String title, String author, String publisher, int copyWriteDate){
		this.title = title;
		this.author = author;
		this.publisher = publisher;
		this.copyWriteDate = copyWriteDate;
	}
	public String getTitle(){
		return title;
		}
	public String getAuthor(){
		return author;
		}
	public String getPublisher(){
		return publisher;
		}
	public int getCopyWriteDate(){
		return copyWriteDate;
		}
	public void setTitle(String title){
		this.title = title;
	}
	public void setAuthor(String author){
		this.author = author;
	}
	public void setPublisher(String publisher){
		this.publisher = publisher;
	}
	public void setCopyWriteDate(int copyWriteDate){
		this.copyWriteDate = copyWriteDate;
	}
	
	public String toString(){
		String dis = "";
		dis+= "Title: \t"+title+"\n";
		dis+= "Author: \t"+author+"\n";
		dis+= "Publisher: \t"+publisher+"\n";
		dis+= "Copy write date: \t"+copyWriteDate+"\n";
		return dis;
	}
	

}
