package Dice;

public class PairOfDie {
	private Die die1, die2;
	
	public PairOfDie(){
		die1 = new Die();
		die2 = new Die();
	}
	
	public int getDie1(){
		return die1.getFaceVlaue();
	}
	public int getDie2(){
		return die2.getFaceVlaue();
	}
	
	public void rollBoth(){
		die1.roll();
		die2.roll();
	}
	
}
