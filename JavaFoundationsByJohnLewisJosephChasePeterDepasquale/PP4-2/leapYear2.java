//Quinten Becker 2015
import java.util.Scanner;

public class leapYear2 {
	
	
	public static void main(String args[]){
		int year; //varable to store year value.
		String answer = "y";
		boolean checkAgain = true; //var to see if user wants to play again
		Scanner input = new Scanner(System.in);
		//varable to store year value.
		do{
			System.out.println("Please enter a year: ");
			Scanner scan = new Scanner(System.in);
			year = scan.nextInt();
			if(year > 1582){
				if((year % 400 == 0)||(year %4 ==0)&&(year % 100 != 0)){
					System.out.println("Year "+ year +" is a leap year!");
				}else{
					System.out.println("Year " + year + " is not a leap year.");
				}
			}else{
				System.out.println("Sorry, that year was before the gregorian calandar was made");
			}
			
			System.out.println("Would you like to check another year? (Y/N)");
			answer = input.nextLine();
			
			
			
			
			//scan.close();
		}while(!answer.equalsIgnoreCase("N"));
		
		
	
	}
}