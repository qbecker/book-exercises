
public class Dog {
	private String name;
	private int age;
	
	public Dog(String name, int age){
		this.name = name;
		this.age = age;
	}
	
	public String getName(){
		return name;
		}
	public int getAge(){
		return age;
	}
	public int getPersonYears(){
		return age*7;
	}
	public String toString(){
		return "the name of the dog is "+getName()+
				" the age of the dog is "+getAge()+
				" the dog is "+getPersonYears();
	}
}
