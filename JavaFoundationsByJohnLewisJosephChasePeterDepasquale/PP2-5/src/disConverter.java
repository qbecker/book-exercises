import java.util.*;
//Quinten Becker

public class disConverter {
	public static void main(String args[]){
		Scanner miles = new Scanner(System.in);
		// initalize Scanner
		
		float input;
		float km = (float) .62137;
		float output;
		// miles and kilimeter varables
		System.out.println("|===============================|");
		System.out.println("|  Miles to kilometers converter|");
		System.out.println("|===============================|");
		
		System.out.println("Enter a number to conver: (in miles)");
		input = miles.nextFloat();
		// take in input in miles
		
		output = input/km;
		//do some math
		
		System.out.println(input +" miles to kilometers is: "+ output);
		miles.close();
	}

}
