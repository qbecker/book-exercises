import java.util.*;
import java.lang.*;
public class reverseTime {
	public static void main (String args[]){
		//declare seconds
		int secInput, hours, min,sec;
		float temp, result;
		
		
		Scanner input = new Scanner(System.in); // input Scanner
		System.out.println("|===============================|");
		System.out.println("|  seconds to time converter    |");
		System.out.println("|===============================|");
		
		System.out.println("Enter a number of seconds: ");//ask for input
		
		secInput = input.nextInt();//Store input
		
		hours = (int) Math.floor(secInput/3600); //doing some math....
		
		min = (int) Math.floor((secInput -(hours*3600))/60);//doing some math....
		
		sec = (int) Math.floor(secInput % 60);//doing some math....
		
		
		//Feed back desired information
		
		System.out.println(secInput+ " seconds converted is "+hours+" hours "+ min+" minuits, and "+sec+" seconds");
		
	}

}
