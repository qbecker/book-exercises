//Quinten Becker 2015
import java.util.Scanner;

public class leapYear {
	
	
	public static void main(String args[]){
		int year;
		//varable to store year value.
		System.out.println("Please enter a year: ");
		Scanner scan = new Scanner(System.in);
		year = scan.nextInt();
		
		//check for less than 1582 
		if(year > 1582){
			if((year % 400 == 0)||(year %4 ==0)&&(year % 100 != 0)){
				System.out.println("Year "+ year +" is a leap year!");
			}else{
				System.out.println("Year " + year + " is not a leap year.");
			}
		}else{
			System.out.println("Sorry, that year was before the gregorian calandar was made");
		}
		scan.close();
		}
	
	}
