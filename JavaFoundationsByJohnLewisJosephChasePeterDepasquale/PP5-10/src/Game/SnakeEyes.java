package Game;

import Dice.PairOfDie;

public class SnakeEyes {
	final int ROLLS = 500;
	int count = 0;
	PairOfDie pair = new PairOfDie();
	
	public int playGame(){
		count = 0;
		for(int i = 0; i <= ROLLS; i ++){
			pair.rollBoth();
			if(pair.getDie1() == 1 && pair.getDie2() == 1){
				count ++;
			}
			
		}
		System.out.println("Number of Rolls: " + ROLLS);
		System.out.println("Number of SnakeEyes: " + count);
		System.out.println("Ratio: " + (float)count/ROLLS);
		
		return count;
	}
	
	
}
