
public class Bulb {
	private boolean lightSwitch;
	
	public Bulb(){}

	public boolean isLightSwitch() {
		return lightSwitch;
	}

	public void setLightSwitch(boolean lightSwitch) {
		this.lightSwitch = lightSwitch;
	}
	
	public void turnOn(){
		lightSwitch = true;
	}
	
	public void turnOff(){
		lightSwitch = false;
	}
	public void isOnOrOff(){
		if(lightSwitch == true){
			System.out.println("The light is on");
		}else{
			System.out.println("The light is off");
		}
	}
}
