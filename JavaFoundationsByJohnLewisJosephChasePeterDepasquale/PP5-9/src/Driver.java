
public class Driver {
	public static void main(String args[]){
		Bulb test1 = new Bulb();
		Bulb test2 = new Bulb();
		Bulb test3 = new Bulb();
		
		test1.setLightSwitch(true);
		test2.setLightSwitch(false);
		test3.setLightSwitch(true);
		
		test1.turnOn();
		test2.turnOff();
		test3.turnOff();
		
		test1.isOnOrOff();
		test2.isOnOrOff();
		test3.isOnOrOff();
	
	}
	
}
